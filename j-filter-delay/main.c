#include <stdlib.h>
#include <string.h>
#include <ladspa.h>
#include <math.h>

#define PORT_COUNT 6

#define JD_PAR_1   0
#define JD_PAR_2   1
#define JD_PAR_3   2
#define JD_PAR_4   3
#define JD_INPUT   4
#define JD_OUTPUT  5

#define MAX_DELAY 2

typedef struct {
  LADSPA_Data * delay_length;
  LADSPA_Data * delay_buffer;
  unsigned long prev_length;
  unsigned long buffer_size;
  unsigned long write_pointer;
  unsigned long read_pointer;
} Delay_line;

typedef struct {
  LADSPA_Data * parameter_q;
  LADSPA_Data * parameter_f;
  LADSPA_Data * delay_mix;
  LADSPA_Data * input;
  LADSPA_Data * output;
  Delay_line d0;
  float feedback;
  float buffer_0;
  float buffer_1;
  unsigned long sample_rate;
} Plugini;

static LADSPA_Handle instantiate(const LADSPA_Descriptor * descriptor,
				 unsigned long sampleRate) {
  Plugini * p;
  p = (Plugini *)malloc(sizeof(Plugini));
  if(p == NULL){ return NULL; }

  p->sample_rate = sampleRate;

  unsigned long minimum_buffer_size;
  minimum_buffer_size = (unsigned long)(p->sample_rate * MAX_DELAY);

  p->d0.buffer_size = 1;
  while(p->d0.buffer_size < minimum_buffer_size){
    p->d0.buffer_size = 2 * p->d0.buffer_size;
  }

  p->d0.delay_buffer = (LADSPA_Data *)calloc(p->d0.buffer_size,
					     sizeof(LADSPA_Data));

  p->d0.write_pointer = 0;
  p->d0.read_pointer = 0;
  p->d0.prev_length = 0;
  
  return p;
}

static void connect_port(LADSPA_Handle handle, unsigned long port, LADSPA_Data * data){
  Plugini * p = (Plugini *)handle;

  switch (port) {
  case JD_PAR_1:
    p->parameter_q = data;
    break;
  case JD_PAR_2:
    p->parameter_f = data;
    break;
  case JD_PAR_3:
    p->delay_mix = data;
    break;
  case JD_PAR_4:
    p->d0.delay_length = data;
    break;
  case JD_INPUT:
    p->input = data;
    break;
  case JD_OUTPUT:
    p->output = data;
    break;
  }
}

static void activate(LADSPA_Handle handle) {
  Plugini * p  = (Plugini *)handle;

  memset(p->d0.delay_buffer, 0, sizeof(LADSPA_Data) * p->d0.buffer_size);

  p->feedback = 0.01f;
  p->buffer_0 = 0.01f;
  p->buffer_1 = 0.01f;
}

static void run(LADSPA_Handle handle, unsigned long sample_count) {
  Plugini * p = (Plugini *)handle;

  float par_f = *p->parameter_f;
  float par_q = *p->parameter_q;

  float delay0 = *(p->d0.delay_length);
  float mix    = *(p->delay_mix);
  float dry    = 1.0 - mix;

  unsigned long delay_samples0;
  delay_samples0 = (unsigned long)((LADSPA_Data)p->sample_rate * delay0);

  if(delay_samples0 != p->d0.prev_length){
    p->d0.write_pointer = p->d0.read_pointer + delay_samples0;
    p->d0.prev_length = delay_samples0;
  }

  p->feedback = par_q + par_q / (1.0f - par_f);

  for(unsigned long i = 0; i < sample_count; i++){

    if(p->d0.write_pointer >= p->d0.buffer_size){
      p->d0.write_pointer = p->d0.write_pointer - p->d0.buffer_size;
    }
    if(p->d0.read_pointer >= p->d0.buffer_size){
      p->d0.read_pointer = p->d0.read_pointer - p->d0.buffer_size;
    }

    // calculate filtered value
    p->buffer_0 = (p->buffer_0 + par_f * (p->input[i] - p->buffer_0 +
					  (p->feedback * (p->buffer_0 - p->buffer_1))));
    p->buffer_1 = (p->buffer_1 + par_f * (p->buffer_0 - p->buffer_1));

    // place filtered value to delay buffer
    p->d0.delay_buffer[p->d0.write_pointer] = p->buffer_1;

    // mix delayed and dry sample to output
    p->output[i] = (mix * p->d0.delay_buffer[p->d0.read_pointer]) + (dry * p->input[i]);

    p->d0.write_pointer++;
    p->d0.read_pointer++;
  }
}

static void cleanup(LADSPA_Handle handle) {
  Plugini * p = (Plugini *)handle;
  free(p->d0.delay_buffer);
  free(handle);
}

static LADSPA_Descriptor * descriptor = NULL;

static void __attribute__ ((constructor)) init() {
  LADSPA_PortDescriptor * portDescriptors;
  LADSPA_PortRangeHint * portRangeHints;
  char ** portNames;

  descriptor = (LADSPA_Descriptor *)malloc(sizeof(LADSPA_Descriptor));

  if(!descriptor){ return; }

  descriptor->UniqueID  = 1065; // should be unique
  descriptor->Label     = strdup("suodatin");
  descriptor->Name      = strdup("filtered delay");
  descriptor->Maker     = strdup("Jani V Anttila");
  descriptor->Copyright = strdup("None");

  descriptor->Properties = LADSPA_PROPERTY_HARD_RT_CAPABLE;

  descriptor->PortCount = PORT_COUNT;

  portDescriptors
    = (LADSPA_PortDescriptor *) calloc(PORT_COUNT, sizeof(LADSPA_PortDescriptor));

  portDescriptors[JD_PAR_1]  = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
  portDescriptors[JD_PAR_2]  = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
  portDescriptors[JD_PAR_3]  = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
  portDescriptors[JD_PAR_4]  = LADSPA_PORT_INPUT | LADSPA_PORT_CONTROL;
  portDescriptors[JD_INPUT]  = LADSPA_PORT_INPUT | LADSPA_PORT_AUDIO;
  portDescriptors[JD_OUTPUT] = LADSPA_PORT_OUTPUT | LADSPA_PORT_AUDIO;

  descriptor->PortDescriptors = portDescriptors;

  portNames = (char **) calloc(PORT_COUNT, sizeof(char *));
  portNames[JD_PAR_1]  = strdup("par f");
  portNames[JD_PAR_2]  = strdup("par q");
  portNames[JD_PAR_3]  = strdup("delay mix");
  portNames[JD_PAR_4]  = strdup("d0 length");
  portNames[JD_INPUT]  = strdup("Input");
  portNames[JD_OUTPUT] = strdup("Output");

  descriptor->PortNames = (const char * const *) portNames;

  portRangeHints
    = (LADSPA_PortRangeHint *) calloc(PORT_COUNT, sizeof(LADSPA_PortRangeHint));

  portRangeHints[JD_PAR_1].HintDescriptor
    = LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE | LADSPA_HINT_DEFAULT_LOW;
  portRangeHints[JD_PAR_1].LowerBound = 0.1;
  portRangeHints[JD_PAR_1].UpperBound = 0.98;
  portRangeHints[JD_PAR_2].HintDescriptor
    = LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE | LADSPA_HINT_DEFAULT_HIGH;
  portRangeHints[JD_PAR_2].LowerBound = 0.0001;
  portRangeHints[JD_PAR_2].UpperBound = 0.9999;
  portRangeHints[JD_PAR_3].HintDescriptor
    = LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE | LADSPA_HINT_DEFAULT_0;
  portRangeHints[JD_PAR_3].LowerBound = 0.0;
  portRangeHints[JD_PAR_3].UpperBound = 1.0;
  portRangeHints[JD_PAR_4].HintDescriptor
    = LADSPA_HINT_BOUNDED_BELOW | LADSPA_HINT_BOUNDED_ABOVE | LADSPA_HINT_DEFAULT_1;
  portRangeHints[JD_PAR_4].LowerBound = 0.01;
  portRangeHints[JD_PAR_4].UpperBound = 2.0;

  portRangeHints[JD_INPUT].HintDescriptor = 0;
  portRangeHints[JD_OUTPUT].HintDescriptor = 0;

  descriptor->PortRangeHints      = portRangeHints;
  descriptor->instantiate         = instantiate;
  descriptor->connect_port        = connect_port;
  descriptor->activate            = activate;
  descriptor->run                 = run;
  descriptor->run_adding          = NULL;
  descriptor->set_run_adding_gain = NULL;
  descriptor->deactivate          = NULL;
  descriptor->cleanup             = cleanup;
}

static void __attribute__ ((destructor)) fini() {
  if (descriptor == NULL) return;

  free((char *) descriptor->Label);
  free((char *) descriptor->Name);
  free((char *) descriptor->Maker);
  free((char *) descriptor->Copyright);
  free((char *) descriptor->PortDescriptors);

  for(int i = 0; i < PORT_COUNT; i++){
    free((char *) descriptor->PortNames[i]);
  }

  free((char **) descriptor->PortNames);
  free((LADSPA_PortRangeHint *) descriptor->PortRangeHints);
  free(descriptor);
}

const LADSPA_Descriptor * ladspa_descriptor(unsigned long index) {
  if(index != 0){ return NULL; }
  return descriptor;
}
